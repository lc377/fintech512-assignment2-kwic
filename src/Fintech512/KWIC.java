package Fintech512;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class KWIC {
    public static ArrayList<String> GenerateKeywords(ArrayList<String> titles) {
        HashSet<String> keywords = new HashSet<>();
        for (String title : titles) {
            for (String word : title.split(" ")) {
                keywords.add(word.toLowerCase());
            }
        }

        return new ArrayList<>(keywords);
    }

    public static ArrayList<String> SearchFilteredKeywords(ArrayList<String> keywords, ArrayList<String> ignoredwords) {
        ArrayList<String> filteredkeywords = (ArrayList<String>) keywords.clone();
        filteredkeywords.removeAll(ignoredwords);
        return filteredkeywords;
    }

    public static ArrayList<String> SortKeywords(ArrayList<String> keywords) {
        ArrayList<String> sortkeywords = (ArrayList<String>) keywords.clone();
        Collections.sort(sortkeywords);
        return sortkeywords;
    }

    public static ArrayList<String> ListKeywordTitles(ArrayList<String> keywords, ArrayList<String> titles) {
        ArrayList<String> keywordtitles = new ArrayList<>();
        for (String keyword : keywords) {
            keyword = keyword.toLowerCase();
            for (String title : titles){
                title = title.toLowerCase();
                Matcher m = Pattern.compile("\\b" + keyword + "\\b").matcher(title);
                while (m.find()) {
                    keywordtitles.add(title.substring(0, m.start())
                            + keyword.toUpperCase() + title.substring(m.end()));
                }
            }
        }
        return keywordtitles;
    }

    public static ArrayList<String> GenerateKWICindex(ArrayList<String> titles, ArrayList<String> ignoredwords) {
        ArrayList<String> keywords = GenerateKeywords(titles);
        ArrayList<String> filteredkeywords = SearchFilteredKeywords(keywords, ignoredwords);
        ArrayList<String> sortedkeywords = SortKeywords(filteredkeywords);
        ArrayList<String> keyworldtitlelist = ListKeywordTitles(sortedkeywords, titles);

        return keyworldtitlelist;
    }

    public static void ParseInput(String Input, ArrayList<String> ignoredwords, ArrayList<String> titles) {
        String[] lines = Input.split("[\\r?\\n|\\r]+");
        Boolean ignoredWordsdone = false;
        for(String line: lines){
            if (line.equals("::")) {
                ignoredWordsdone = true;
                continue;
            }
            if (!ignoredWordsdone) {
                ignoredwords.add(line);
            } else {
                titles.add(line);
            }

        }
    }

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        String line;
        ArrayList<String> ignoredwords = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();
        try{
            while ((line = br.readLine()) != null){
                input += line + "\n";
            }
            KWIC.ParseInput(input, ignoredwords, titles);
            ArrayList<String> output = KWIC.GenerateKWICindex(titles, ignoredwords);
            for (String title: output) {
                System.out.println(title);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

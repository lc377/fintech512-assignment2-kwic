import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;
import Fintech512.KWIC;

public class KWICTests {
    @Test
    public void testGenerateKeyword(){
        ArrayList<String> titles = new ArrayList<String>();
        titles.add("The Old Man and The Sea");
        titles.add("Descent of Man");
        titles.add("The Ascent of Man");
        titles.add("A Portrait of The Artist As a Young Man");

        ArrayList<String> expectedKeywords = new ArrayList<String>(Arrays.asList(
                "the", "old", "man", "and", "sea", "descent", "of", "ascent", "a", "portrait", "artist", "as", "young"
                ));

        ArrayList<String> actualKeywords = KWIC.GenerateKeywords(titles);

        Collections.sort(expectedKeywords);
        Collections.sort(actualKeywords);
        assertArrayEquals(expectedKeywords.toArray(), actualKeywords.toArray());
    }

    @Test
    public void testFilteredKeyword(){
        ArrayList<String> ignoredword = new ArrayList<>(Arrays.asList(
                "the", "of", "and", "a", "as"
        ));

        ArrayList<String> keywords = new ArrayList<>(Arrays.asList(
                "the", "old", "man", "and", "sea", "descent", "of", "ascent", "a", "portrait", "artist", "as", "young"
        ));

        ArrayList<String> expectedFilteredKeywords = new ArrayList<>(Arrays.asList(
                "old", "man", "sea", "descent", "ascent", "portrait", "artist", "young"
        ));

        ArrayList<String> actualFilteredKeywords = KWIC.SearchFilteredKeywords(keywords, ignoredword);

        Collections.sort(expectedFilteredKeywords);
        Collections.sort(actualFilteredKeywords);
        assertArrayEquals(expectedFilteredKeywords.toArray(), actualFilteredKeywords.toArray());

    }

    @Test
    public void testSortKeywords(){
        ArrayList<String> keywords = new ArrayList<>(Arrays.asList(
                "old", "man", "sea", "descent", "ascent", "portrait", "artist", "young"
        ));

        ArrayList<String> expectedSortedKeywords = new ArrayList<>(Arrays.asList(
                "artist", "ascent", "descent", "man", "old", "portrait", "sea", "young"
        ));

        ArrayList<String> actualSortedKeywords = KWIC.SortKeywords(keywords);
        assertArrayEquals(expectedSortedKeywords.toArray(), actualSortedKeywords.toArray());

    }

    @Test
    public void testListKeywordTitles() {
        ArrayList<String> keywords = new ArrayList<>(Arrays.asList(
                "artist", "ascent", "descent", "man", "old", "portrait", "sea", "young"
        ));

        ArrayList<String> titles = new ArrayList<String>(Arrays.asList(
                "The Old Man and The Sea",
                "Descent of Man",
                "The Ascent of Man",
                "A Portrait of The Artist As a Young Man",
                "a man and a Man"
        ));

        ArrayList<String> expectedKeywordtitles = new ArrayList<String>(Arrays.asList(
                "a portrait of the ARTIST as a young man",
                "the ASCENT of man",
                "DESCENT of man",
                "the old MAN and the sea",
                "descent of MAN",
                "the ascent of MAN",
                "a portrait of the artist as a young MAN",
                "a MAN and a man",
                "a man and a MAN",
                "the OLD man and the sea",
                "a PORTRAIT of the artist as a young man",
                "the old man and the SEA",
                "a portrait of the artist as a YOUNG man"
        ));

        ArrayList<String> actualKeywordtitles = KWIC.ListKeywordTitles(keywords, titles);
        assertArrayEquals(expectedKeywordtitles.toArray(), actualKeywordtitles.toArray());
    }

    @Test
    public void testGenerateKWICindex() {
        ArrayList<String> titles = new ArrayList<>(Arrays.asList(
                "Descent of Man",
                "The Ascent of Man",
                "The Old Man and The Sea",
                "A Portrait of The Artist As a Young Man",
                "A Man is a Man but Bubblesort IS A DOG"
        ));

        ArrayList<String> ignoredwords = new ArrayList<>(Arrays.asList(
                "is", "the", "of", "and", "as", "a", "but"
        ));

        ArrayList<String> expectedKWICindex = new ArrayList<>(Arrays.asList(
                "a portrait of the ARTIST as a young man",
                "the ASCENT of man",
                "a man is a man but BUBBLESORT is a dog",
                "DESCENT of man",
                "a man is a man but bubblesort is a DOG",
                "descent of MAN",
                "the ascent of MAN",
                "the old MAN and the sea",
                "a portrait of the artist as a young MAN",
                "a MAN is a man but bubblesort is a dog",
                "a man is a MAN but bubblesort is a dog",
                "the OLD man and the sea",
                "a PORTRAIT of the artist as a young man",
                "the old man and the SEA",
                "a portrait of the artist as a YOUNG man"
        ));

        ArrayList<String> actualKWICindex = KWIC.GenerateKWICindex(titles,ignoredwords);
        assertArrayEquals(expectedKWICindex.toArray(), actualKWICindex.toArray());
    }

    @Test
    public void testParseInputString(){
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";

        ArrayList<String> expectedignoredwords = new ArrayList<>(Arrays.asList(
                "is", "the", "of", "and", "as", "a", "but"
        ));

        ArrayList<String> expectedtitles = new ArrayList<>(Arrays.asList(
                "Descent of Man",
                "The Ascent of Man",
                "The Old Man and The Sea",
                "A Portrait of The Artist As a Young Man",
                "A Man is a Man but Bubblesort IS A DOG"
        ));

        ArrayList<String> actualignoredwords = new ArrayList<>();
        ArrayList<String> actualtitles = new ArrayList<>();
        KWIC.ParseInput(input, actualignoredwords, actualtitles);
        assertArrayEquals(expectedignoredwords.toArray(), actualignoredwords.toArray());
        assertArrayEquals(expectedtitles.toArray(), actualtitles.toArray());
    }

}
